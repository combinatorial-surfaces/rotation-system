#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["ConnectedComponent"]

import collections

from . import basic
from . import core

class ConnectedComponent():
    """Create an object representing a connected component

     * It can be empty (if you pass 'None' to the constructor). In that case
       walks yield nothing and copies are empty. You can test that case by
       checking the truth value of the component.
     * Else, you must pass an instance of Flag (or a subclass) to the
       constructor, to get that flag's component."""
    __slots__ = ("_anchor_",)

    def __init__(self, anchor):
        """Create an object representing a connected component
         * if 'anchor' is None, the component is empty;
         * else, 'anchor' must be an instance of Flag (or a subclass),
           and the new object will represent that flag's component."""
        if anchor is not None and not isinstance(anchor, core.Flag):
            raise TypeError("impossible to create the component of '{}' "
                    "object".format(type(anchor).__name__))
        self._anchor_ = anchor

    @property
    def anchor(self):
        return self._anchor_

    def __bool__(self):
        return self._anchor_ is not None

    def __repr__(self):
        return "{}({!r})".format(type(self).__name__, self._anchor_)

    def _walk_(self, check_visit, depth_first, cycle_first):
        if self._anchor_ is None: return iter(())
        if check_visit is None:
            def check_visit(flag):
                try:
                    return flag.persist[check_visit]
                except KeyError:
                    flag.persist[check_visit] = True
                    flag.side.persist[check_visit] = True
                return False
        return self._walk_impl_(check_visit, depth_first, cycle_first)

    def _walk_impl_(self, check_visit, depth_first, cycle_first):
        # queue of (flag, <reached by twin>) pairs
        pending = collections.deque(((self._anchor_, False),))
        # ordering of flags in the queue (cycle_first=True for BFS)
        cycle_first = cycle_first or not depth_first
        append_twin = pending.append if depth_first else pending.appendleft
        append_next = pending.append if cycle_first else pending.appendleft
        # the walk itself
        while pending:
            flag, from_twin = pending.pop()
            if check_visit(flag): continue
            yield (flag, from_twin)
            if not from_twin: append_twin((flag.twin, True))
            append_next((flag.next, False))

    def walk(self, depth_first = False, cycle_first = False,
                   both_sides=False):
        """Return an iterator walking along the connected component.
        Unless both_sides is True, this iterator will only yield one of 'flag'
        and 'flag.side' for any given flag.
        If 'depth_first' is False, the cycle of any visited flag is visited in
        priority, then other flags in the order of the visit of their twins:
        all directly connected vertices will be reached before second order
        ones, etc. In that case 'cycle_first' is ignored.
        If 'depth_first' is true, then twins added in the queue will be visited
        in reverse order (last-in first-out), which means that vertices nearing
        recently seen vertices will be visited first. If 'cycle_first' is True
        then a flag cycle() will be nevertheless visited in priority, but if
        'cycle_first' is False, twins will be walked through (and a new vertex
        reached) before the full cycle() is walked."""
        for flag, _ in self._walk_(None, depth_first, cycle_first):
            yield flag
            if both_sides: yield flag.side

    def __iter__(self):
        return self.walk()

    def spanning_tree(self, depth_first = False):
        """Iterate along a spanning tree, rooted at 'self.anchor'.
        It will yield only one direction of each edge (that is at most one of
        'flag' and 'flag.twin'), which corresponds to the half-edge going
        away from the root in the tree.
        Even if the graph is non-orientable, this iterator will yield at most
        one of 'flag' and 'flag.side' for any given flag."""
        for flag, from_twin in self._walk_(None, depth_first, True):
            if from_twin: yield flag.twin

    def vertices(self):
        if not self._anchor_: return
        yield self._anchor_
        for flag, from_twin in self._walk_(None, False, False):
            if from_twin: yield flag

    def is_orientable(self):
        def check_visit(flag):
            try:
                return flag.persist[check_visit]
            except KeyError:
                flag.persist[check_visit] = True
            return False
        for flag, _ in self._walk_(check_visit, False, False):
            if flag.side.persist.get(check_visit): return False
        return True

    def copy_iter(self, *, Flag=basic.BasicFlag):
        """Walk and copy the connected component, yielding (original, copy)
        Flag pairs for each original Flag walked through.

        The iterator has to be exhausted for the copy to be complete."""
        def user(): return # a persist-compatible object only living here
        for flag in self:
            try:
                copied = flag.persist[user]
            except KeyError:
                copied = flag.persist[user] = Flag()
                flag.side.persist[user] = copied.side
                flag.twin.persist[user] = copied.twin
                flag.twin.side.persist[user] = copied.twin.side
            try:
                prev_copied = flag.prev.persist[user]
            except KeyError:
                pass
            else:
                if copied.prev != prev_copied:
                    copied.attach(prev_copied)
            yield (flag, copied)

    def copy(self, *, Flag=basic.BasicFlag):
        """Copy the connected component structure, returning a
        ConnectedComponent anchored at the original anchor's copy."""
        if self._anchor_ is None: return ConnectedComponent(None)
        i = self.copy_iter(Flag=Flag)
        _, new = next(i)
        collections.deque(i, maxlen=0) # exhaust iterator to copy all
        return ConnectedComponent(new)

    @property
    def dual(self):
        "The corresponding connected component in the dual graph"
        if self._anchor_ is None: return ConnectedComponent(None)
        return ConnectedComponent(self._anchor_.dual)
