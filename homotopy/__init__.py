#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["null_homotopy_test"]

from .cover import radial_cover
import collections

def null_homotopy_test(component, path, **kw):
    cover = radial_cover(component, **kw)
    lift = cover.lift(path)
    try:
        first = next(lift)
    except StopIteration:
        return True
    # should never raise because projecting a path onto the radial
    # generates a path whose length is pair, and lifting preserves it.
    last = collections.deque(lift, maxlen=1).pop()
    return first.same_vertex(last.twin)
