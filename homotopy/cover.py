#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["CoverFlag", "RadialCover", "radial_cover"]

from ..core import Flag
from .sparse import SparseArray
from weakref import WeakKeyDictionary

class CoverFlag(Flag):
    __slots__ = ("_vertex_", "_rf_", "_info_")

    # Flag interface

    @property
    def next(self):
        return self._make_(rf = self._rf_.next)

    @property
    def prev(self):
        return self._make_(rf = self._rf_.prev)

    @property
    def side(self):
        return self._make_(rf = self._rf_.side)

    @property
    def twin(self):
        twin = self._maybe_twin_()
        if twin is not None: return twin
        return self._mirror_()

    def _persist_(self, namespace):
        store = self._vertex_.persist.setdefault(self.num, {})
        side = self._rf_.persist[self._info_][2]
        return store.setdefault((side, namespace), WeakKeyDictionary())

    @property
    def connected_component(self):
        return CoverComponent(self)

    # Other members

    class Vertex():
        __slots__ = ("lookup", "persist")

        def __new__(cls, size):
            self = super().__new__(cls)
            self.lookup = SparseArray(size)
            self.persist = SparseArray(size)
            return self

    class CoverInfo(list):
        __slots__ = ("__weakref__")
        __eq__ = object.__eq__
        __hash__ = object.__hash__
        __repr__ = object.__repr__

    def __new__(cls, base):
        info = cls.CoverInfo()
        for f in base.twin.cycle():
            f.persist[info] = (len(info), 1, 1)
            f.side.persist[info] = (len(info), 1, -1)
            info.append(f)
        for t in base.cycle():
            n = t.twin.persist[info][0]
            t.persist[info] = (n, -1, 1)
            t.side.persist[info] = (n, -1, -1)
        self = super().__new__(cls)
        self._vertex_ = cls.Vertex(len(info))
        self._rf_ = base
        self._info_ = info
        return self

    @property
    def num(self):
        return self._rf_.persist[self._info_][0]

    @property
    def vertex_type(self):
        "equal to 1 when self's vertex is a lift of the face center, else -1."
        return self._rf_.persist[self._info_][1]


    def _make_(self, *, vertex=None, rf=None):
        new = super().__new__(type(self))
        new._vertex_ = vertex or self._vertex_
        new._rf_ = rf or self._rf_
        new._info_ = self._info_
        return new

    def _lift_path_(self, path):
        cur = self
        for flag in path:
            cur = cur._lift_radial_(flag)
            yield cur
            cur = cur.twin

    def _lift_radial_(self, flag):
        _, end, side = self._rf_.persist[self._info_]
        try:
            flag_end = flag.persist[self._info_][1]
        except AttributeError:
            try:
                flag = int(flag)
            except TypeError:
                raise TypeError("not a flag nor a num") from None
            try:
                flag = self._info_[flag]
            except IndexError:
                raise ValueError("invalid number") from None
            if end < 0: flag = flag.twin
            if side * flag.persist[self._info_][2] < 0: flag = flag.side
            flag_end = end
        except KeyError:
            raise ValueError("non-radial flag") from None
        if flag_end != end:
            raise ValueError("cannot lift flag: incompatible cover vertex")
        return self._make_(rf = flag)

    def lift(self, flag_or_path):
        try:
            path = iter(flag_or_path)
        except TypeError:
            pass
        else:
            return self._lift_path_(path)
        return self._lift_radial_(flag_or_path)

    def project(self):
        return self._rf_

    def __eq__(self, other):
        if not (isinstance(other, CoverFlag) and self._info_ is other._info_):
            return NotImplemented
        return (self._vertex_ is other._vertex_) \
                and (self._rf_ == other._rf_)

    def __repr__(self):
        return "<{}: {}, {!r}>".format(type(self).__name__,
                                     hex(id(self._vertex_)),
                                     self._rf_)

    def _maybe_twin_(self):
        try:
            vertex = self._vertex_.lookup[self.num]
        except KeyError:
            return None
        return self._make_(vertex = vertex, rf = self._rf_.twin)

    @property
    def is_real(self):
        return self.num in self._vertex_.lookup

    def _realize_(self, twin_vertex=None):
        assert(not self.is_real)
        if twin_vertex is None:
            twin_vertex = self.Vertex(self._vertex_.lookup.size)
        num = self.num
        self._vertex_.lookup[num] = twin_vertex
        twin_vertex.lookup[num] = self._vertex_
        return twin_vertex

    def _mirror_(self):
        self_v = self._realize_()
        for cur in (self, self.side):
            while True:
                maybe = cur.next._maybe_twin_()
                if maybe is None: break
                nxt = maybe.next; nxt_v = nxt._realize_()
                cur.twin.prev._realize_(nxt_v)
                cur = nxt
        return self._make_(vertex = self_v, rf = self._rf_.twin)

    def same_vertex(self, other):
        return self._vertex_ is other._vertex_

    def real_neighbours(self):
        """Iterate over all flags in self.cycle() where is_real is True.
        The iteration is not in cycle order, but in order of realization."""
        return map(self.lift, self._vertex_.lookup)

    def line(self, both_sides=True, enlarge=0):
        """Iterate over all flags crossing the same dual line.
        If both_sides is False, the iteration will only yield flags crossing
        the half-line on the side of self. If enlarge is greater than 0, self
        if always realized, and the value of enlarge is the number of yet
        unrealized flags that will be yielded after reaching the end of the
        cover region along the line, effectively enlarging the region.
        In other words, |2*enlarge| (or |2*enlarge+1|) additional flags will be
        yielded if enlarge > 0 compared to the default enlarge = 0, if
        both_sides is True (if it is False, then |enlarge| or |enlarge+1|
        flags will be yielded in addition)."""
        if enlarge == 0 and not self.is_real: return
        self.twin # realize if needed
        yield self
        sides = (self, self.side) if both_sides else (self,)
        for cur in sides:
            unreal = 0
            while True:
                nxt = cur.next
                if not nxt.is_real: unreal += 1
                if unreal > enlarge: break
                cur = nxt.twin.next
                yield cur


from ..component import ConnectedComponent
import collections

class CoverComponent(ConnectedComponent):
    __slots__ = ()

    def _walk_impl_(self, check_visit, depth_first, cycle_first):
        # queue of (flag, <reached by twin>) pairs
        pending = collections.deque()
        # ordering of flags in the queue (cycle_first=True for BFS)
        cycle_first = cycle_first or not depth_first
        append_twin = pending.append if depth_first else pending.appendleft
        append_next = pending.append if cycle_first else pending.appendleft
        # the walk itself
        for neigh in self.anchor.real_neighbours():
            append_twin((neigh.twin, True))
        for neigh in self.anchor.real_neighbours():
            append_next((neigh, False))
        while pending:
            flag, from_twin = pending.pop()
            if check_visit(flag): continue
            yield (flag, from_twin)
            if not from_twin: continue
            for neigh in flag.real_neighbours():
                append_twin((neigh.twin, True))
            for neigh in flag.real_neighbours():
                append_next((neigh, False))

from ..basic import BasicFlag
from .reduce import radial

class RadialCover(CoverComponent):
    __slots__ = ("_radial_")

    def __init__(self, source, *, Flag=BasicFlag):
        # compute the radial graph
        rad = self._radial_ = radial(source, Flag=Flag)
        if not rad: return super().__init__(None)
        # create the first lift and anchor the component
        super().__init__(CoverFlag(rad.loop_base))

    @property
    def radial(self):
        return self._radial_

    def project(self, cover_path):
        try:
            return (f.project() for f in cover_path)
        except TypeError:
            return cover_path.project()

    def lift(self, path):
        return self.anchor.lift(self._radial_.project(path))


radial_cover = RadialCover
