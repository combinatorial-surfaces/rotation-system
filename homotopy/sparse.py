#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["SparseArray"]

try:
    import numpy
except ImportError:
    print(
    """WARNING: numpy not available, reverting to dictionnaries.
         O(1) initialisation, insertion and lookup in the worst case is
         not guaranteed anymore (only in average).
         O(n+k1+k2) worst-case complexity for homotopy depends on it."""
    )
    class SparseArray(dict):
        def __init__(self, size):
            self.size = size
else:
    from collections.abc import MutableMapping
    class SparseArray(MutableMapping):
        """A mutable mapping with integer keys between 0 and size-1, with
        O(1) initialisation, insertion and lookup. Based on an idea from
        D. E. Knuth presented in .......
        As with lists, negative indices are tried from the end of the key
        range, that is self[-1] does exactly the same than self[size-1]
        (including raising KeyError when trying to get from an emtpy slot)."""

        __slots__= ("_id_to_slot_", "_slots_")

        def __init__(self, size, values=None):
            self._id_to_slot_ = numpy.empty(size, int)
            self._slots_ = []
            if values is not None: self.update(values)

        def _get_slot_(self, num):
            # map num into the correct range for backref purposes
            size = len(self._id_to_slot_)
            try:
                num = int(num)
            except TypeError:
                raise KeyError(num) from None
            if num < 0: num += size
            if not 0 <= num < size: raise KeyError(num)
            # try to get the contents
            slot = self._id_to_slot_[num]
            try:
                backref, val = self._slots_[slot]
            except IndexError:
                pass
            else:
                if backref == num: return (slot, num, val)
            return (-1, num, None)

        def __getitem__(self, num):
            slot, _, val = self._get_slot_(num)
            if slot < 0: raise KeyError(num)
            return val

        def __setitem__(self, num, value):
            slot, num, _ = self._get_slot_(num)
            if slot >= 0:
                self._slots_[slot] = (num, value)
            else:
                self._slots_.append( (num, value) )
                self._id_to_slot_[num] = len(self._slots_) - 1

        def __delitem__(self, num):
            raise NotImplementedError

        def __len__(self):
            return len(self._slots_)

        def __iter__(self):
            return (num for num, _ in self._slots_)

        def items(self):
            # more efficient than MutableMapping implementation
            return iter(self._slots_)

        @property
        def size(self):
            return len(self._id_to_slot_)

        def __repr__(self):
            return "{}({}, {{{}}})".format(
                    type(self).__name__,
                    len(self._id_to_slot_),
                    ", ".join("{}: {!r}".format(*s) for s in self._slots_))
