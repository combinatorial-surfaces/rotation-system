#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["contract", "radial"]

from .. import core
from ..component import ConnectedComponent
from ..basic import BasicFlag

import itertools

class Contracted(ConnectedComponent):
    __slots__ = ("__weakref__")

    def __init__(self, source, *, Flag=BasicFlag):
        try:
            source = source.connected_component
        except AttributeError:
            pass
        # Make a copy of the initial graph
        for initial, copy in source.copy_iter(Flag=Flag):
            initial.persist[self] = copy
            initial.side.persist[self] = copy.side
            copy.persist[self] = initial
            copy.side.persist[self] = initial.side
        # Contract a spanning tree of the copy so it has only one vertex
        for flag in source.spanning_tree():
            flag.persist[self].contract()
            del flag.persist[self]
            del flag.side.persist[self]
            del flag.twin.persist[self]
            del flag.twin.side.persist[self]
        # Find a non-contracted edge. If there is none the graph itself is a
        # tree, contractible; all paths will be contractible too.
        super().__init__(next(self.project(source), None))

    def project(self, path):
        if isinstance(path, core.Flag):
            return path.persist.get(self)
        return filter(None, (flag.persist.get(self) for flag in path) )

    def preimage(self, flag):
        return flag.persist.get(self)

contract = Contracted



class radial(ConnectedComponent):
    __slots__ = ("__weakref__")

    def __init__(self, source, *, Flag=BasicFlag):
        contracted = contract(source, Flag=Flag)
        if not contracted: return super().__init__(None)
        # Mark edges crossing a spanning tree of the dual; they are "inner
        # edges" of the only face of the polygonal schema and will be
        # dissolved first. Unmarked edges are "borders" and will be removed
        # when the radial graph is complete.
        for d in contracted.dual.spanning_tree():
            f = d.primal; f.persist[self] = f.side.persist[self] = True
            f = f.twin;   f.persist[self] = f.side.persist[self] = True
        # find an edge that we won't dissolve
        def is_inner(flag):
            return flag.persist.get(self, False)
        try:
            start = next(itertools.filterfalse(is_inner, contracted))
        except StopIteration:
            # The surface is homotopic to a disk. All paths are contractible.
            # Leave the projection information empty so that self.project()
            # will always yield nothing.
            return super().__init__(None)
        # construct an intermediate graph with inner edges removed and replaced
        # by "radial" edges, attached to a vertex in the center of the face.
        def set_radial(flag, radial):
            contracted.preimage(flag).persist[self] = radial
        border = []
        prev_radial = None
        current = start
        while True:
            border.append(current)
            # outer vertex --- radial ------ radial.twin --- center vertex
            # create the radial edge
            radial = Flag()
            # the whole fan between current and the following border edge
            # will be projected onto the radial edge, borders included
            set_radial(current, radial)
            flag = current.prev
            while is_inner(flag):
                set_radial(flag, radial)
                set_radial(flag.side, radial)
                flag.detach() # changes current.prev
                flag = current.prev
            current = flag
            set_radial(current.side, radial)
            # attach the created radial edge
            radial.attach(current)
            if prev_radial: radial.twin.attach(prev_radial)
            prev_radial = radial.twin
            # next in the face
            current = current.twin
            if current == start:
                break
        # now remove all remaining non-radial edges
        for f in border:
            f.detach()
            f.twin.detach()
        # and set the component anchor
        super().__init__(prev_radial)

    @property
    def loop_base(self):
        return self.anchor.twin

    @property
    def face_center(self):
        return self.anchor

    @property
    def relator(self):
        rel = []
        avail = "abcdefghijklmnopqrstuvwxyz"
        def x(): return # weakref-able object with a suitable lifetime
        for u in self.face_center.cycle():
            v = u.twin.prev.twin.side
            try:
                rel.append(v.persist[x])
            except KeyError:
                g, *avail = avail
                u.persist[x] = g
                u.next.side.persist[x] = g + "⁻¹"
                rel.append(g)
        return "".join(rel)


    def project(self, path):
        if isinstance(path, core.Flag):
            path = (path,)
        for flag in path:
            try:
                rad1 = flag.persist[self]
            except KeyError:
                flag = flag.side
                try:
                    rad1 = flag.persist[self]
                except KeyError:
                    continue
            yield rad1
            yield flag.twin.side.persist[self].twin
