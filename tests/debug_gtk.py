#!/usr/bin/env python3
# vim: spelllang=en_us

__all__ = ["GtkView"]

from gi.repository import GObject, Gtk, GLib, Gdk
import threading, weakref

import cairo

from .hyperbolic.tiling.gtk import GtkTiling
from .hyperbolic.cyclic.gtk import GtkBandTiling

GObject.threads_init()

class GtkView(GtkTiling):
    def __init__(self, *a, **kw):
        self.inhibit_band_update = False
        super().__init__(*a, **kw)
        self.add_events(Gdk.EventMask.BUTTON_RELEASE_MASK)
        #
        self.resize(600,600)
        self.move(580,0)
        GLib.idle_add(self.show)
        t = threading.Thread(target=Gtk.main)
        t.daemon = True
        t.start()
        weakref.finalize(t, Gtk.main_quit)

    def edge_props(self, cf):
        return cf.persist.setdefault(self, {})

    def hl(self, path_or_flag, value=True):
        try:
            i = iter(path_or_flag)
        except TypeError:
            self.edge_props(path_or_flag)["hl"] = value
            self.queue_update()
            return
        try:
            first = next(i)
        except StopIteration:
            return
        self.edge_props(first.twin.side)["hl"] = value
        for cf in i:
            self.edge_props(cf)["hl"] = \
                    self.edge_props(cf.twin.side)["hl"] = value
        last = cf.twin.lift(first.num)
        last.twin # realize it
        self.edge_props(last)["hl"] = value
        self.queue_update()

    def unhl(self, cf=None):
        if cf is None:
            comp = self.tiling.base.connected_component
            for cf in comp.walk(both_sides = True):
                self.unhl(cf)
        else:
            try:
                del self.edge_props(cf)["hl"]
            except KeyError:
                pass
        self.queue_update()

    def queue_update(self):
        self.needs_update = True
        GLib.idle_add(self.maybe_update)

    def maybe_update(self):
        if self.needs_update:
            self.needs_update = False
            self.update()

    def scale_render(self, ctx, scale):
        super().scale_render(ctx, scale)
        ctx.set_line_width(2)
        ctx.select_font_face("Sans",
                             cairo.FontSlant.NORMAL,
                             cairo.FontWeight.NORMAL)
        ctx.set_font_size(10)
        comp = self.tiling.base.connected_component
        for cf in comp.walk(both_sides = True):
            try:
                elts = self.tiling.elements(cf)
            except:
                break
            e_p = self.edge_props(cf)
            if "hl" in e_p:
                corners = self.tiling.elements(cf.next).cell.corners
                q = corners[0]
                if not q in elts.cell.corners: q = corners[1]
                p = elts.vertex; p += 0.15 * (q - p)
                ctx.move_to(p.x * scale, p.y * scale)
                p = elts.crossing; p += 0.15 * (q - p)
                ctx.line_to(p.x * scale, p.y * scale)
                ctx.set_source_rgba(0,0,1,1)
                ctx.stroke()
            try:
                ln = e_p["line_num"]
            except:
                pass
            else:
                for p in elts.cell.corners:
                    x = (p.x + elts.crossing.x) / 2
                    y = (p.y + elts.crossing.y) / 2
                    x, y = ctx.user_to_device(x * scale, y * scale)
                    ctx.save()
                    ctx.identity_matrix()
                    ctx.move_to(x, y)
                    ctx.set_source_rgba(1,0,0,1)
                    ctx.show_text(str(ln))
                    ctx.restore()

    def update(self, *a, **kw):
        super().update(*a, **kw)
        if not self.inhibit_band_update:
            GLib.idle_add(self._update_band_)

    def _update_band_(self):
        try:
            band = self.band
        except AttributeError:
            return
        settings = getattr(self, "band_settings", {})
        try:
            bandview = self.bandview
        except AttributeError:
            self.bandview = GtkBandView(self.tiling, *band, **settings)
        else:
            bandview.update(self.tiling, *band, **settings)

    def do_button_release_event(self, event):
        GLib.idle_add(self._update_band_)

    def do_motion_notify_event(self, event):
        self.inhibit_band_update = True
        super().do_motion_notify_event(event)
        self.inhibit_band_update = False

class GtkBandView(GtkBandTiling):
    def update(self, *a, **kw):
        self.show()
        super().update(*a, **kw)
