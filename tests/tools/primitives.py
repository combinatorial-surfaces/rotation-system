#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["chain", "stitch", "quad_band", "tube", "orientable"]

from ...basic import BasicFlag
from .named import NamedFlag

import collections

class chain(list):
    __slots__ = ()

    def __init__(self, count, *, Flag=BasicFlag):
        start = Flag()
        self.append(start)
        end = start.twin
        for _ in range(1, count):
            nxt = Flag()
            end.attach(nxt)
            self.append(nxt)
            end = nxt.twin

    @property
    def start(self):
        return self[0]

    @property
    def end(self):
        return self[-1].twin

    @property
    def connected_component(self):
        return self[0].connected_component

    @classmethod
    def named(cls, count, prefix, *args, Flag=NamedFlag, **kwargs):
        chain = cls(count, *args, Flag=Flag, **kwargs)
        for flag in chain: flag.name = prefix
        return chain

    def __repr__(self):
        return "<{}: {}>".format(type(self).__name__, super().__repr__())

def stitch(u, v):
    for a, b in zip(u, v):
        f = type(a)()
        f.attach(a)
        f.twin.attach(b.prev)

class quad_band(list):
    __slots__ = ()

    def __init__(self, x, y, *, Flag=BasicFlag):
        cur = chain(x, Flag=Flag)
        self.append(cur)
        for _ in range(y):
            nxt = chain(x, Flag=Flag)
            self.append(nxt)
            stitch(cur, nxt)
            cur = nxt

    def hflag(self, x, y):
        return self[y][x]

    def vflag(self, x, y):
        if y == len(self) - 1:
            raise IndexError(y)
        return self[y][x].next

    @property
    def connected_component(self):
        return self[0][0].connected_component

    @classmethod
    def named(cls, x, y, prefix="band", *args, Flag=NamedFlag, **kwargs):
        band = cls(x, y, *args, Flag=Flag, **kwargs)
        for i in range(x):
            for j in range(y+1):
                band.hflag(i, j).name = "{}{}.{}h".format(prefix, i, j)
                band.hflag(i, j).vname = "{}V{}.{}".format(prefix, i, j)
            for j in range(y):
                band.vflag(i, j).name = "{}{}.{}v".format(prefix, i, j)
        return band

    def __repr__(self):
        return "<{}: [{}]>".format(type(self).__name__,
                    ", ".join(list.__repr__(c) for c in self))

class tube(quad_band):
    __slots__ = ()

    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        for i in range(len(self)-1):
            self[i].end.attach(self[i].start.next)
        self[-1].end.attach(self[-1].start) # no vertical edge to skip

    @classmethod
    def named(cls, x, y, prefix="tube", *a, **kw):
        return super().named(x, y, prefix, *a, **kw)

class orientable():
    @staticmethod
    def _create_(genus, loop_length, Flag, namer):
        def loop(c):
            ch = chain(loop_length, Flag=Flag)
            if callable(namer): namer(ch, c)
            return ch
        prev = None
        for g in range(genus,0,-1):
            loop1 = loop(g*2-2); loop2 = loop(g*2-1)
            if prev is not None:
                loop2.start.attach(prev)
            loop1.end.attach(loop2.start)
            loop2.end.attach(loop1.end)
            loop1.start.attach(loop2.end)
            prev = loop1.start
        return prev

    def __call__(self, genus, loop_length=1, *, Flag=BasicFlag):
        return self._create_(genus, loop_length, Flag, None)

    def named(self, genus, loop_length=1, *, Flag=NamedFlag):
        names = "abcdefghijklmnopqrstuvwxyz"
        l = (len(names) // 2) * 2
        def namer(ch, c):
            name = names[c % l] + "'" * (c // l)
            for f in ch: f.name = name
        return self._create_(genus, loop_length, Flag, namer)

orientable = orientable()
