#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["SurfaceInfo"]

import collections
from .coverwrap import CoverCCWrap

class SurfaceInfo(collections.namedtuple("SurfaceInfo",
                "vertices faces edges euler orientable genus")):
    def __new__(cls, fc):
        try:
            fc = fc.connected_component
        except AttributeError:
            pass
        try:
            fc.anchor.is_real
        except AttributeError:
            pass
        else:
            fc = CoverCCWrap(fc)
        vert_degrees = collections.Counter(
                sum(1 for _ in f.cycle())
                for f in fc.vertices())
        face_lengths = collections.Counter(
                sum(1 for _ in f.cycle())
                for f in fc.dual.vertices())
        verts = sum(vert_degrees.values())
        faces = sum(face_lengths.values())
        edges = sum(1 for _ in fc) // 2
        euler = verts - edges + faces
        orientable = fc.is_orientable()
        if orientable:
            g = 1 - euler // 2
        else:
            g = 2 - euler
        return super().__new__(cls,
                (verts, vert_degrees),
                (faces, face_lengths),
                edges,
                euler, orientable, g)

    def __repr__(self):
        return \
"""{0.__class__.__name__}(Vertices: {0.vertices}
  Faces: {0.faces}
  Edges: {0.edges}
  χ = {0.euler} : {1}orientable genus = {0.genus})""".format(
                    self, "" if self.orientable else "non-")
