#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["Edge", "Vertex"]

from ...core import Flag
from weakref import WeakKeyDictionary

class Edge(Flag):
    "A Flag providing a persist-like attribute shared with its side and twin"

    __slots__ = ()

    @property
    def edge(self):
        """Same semantics than Flag.persist, but shared with the other side
        and both sides of self.twin"""
        try:
            return self.persist[Edge]
        except KeyError:
            twin = self.twin
            store = WeakKeyDictionary()
            self.persist[Edge] = self.side.persist[Edge] = store
            twin.persist[Edge] = twin.side.persist[Edge] = store
            return store

class Vertex(Flag):
    """A Flag providing a persist-like attribute shared with all Flags in the
    cycle around the vertex

    The Vertex class assumes that if a Flag's vertex neighbourhood changes,
    it will be notified via:
     * self.attach(other) when self enters other's neighboorhood cycle, and
     * self.detach()      when self starts being alone on its vertex.
    Those need to exist in the base class implementing Flag, but can be no-ops
    just for fallback purposes."""

    __slots__ = ()

    @property
    def vertex(self):
        """Same semantics than Flag.persist, but shared with all flags
        connected together to form a vertex in the graph"""
        try:
            return self.persist[Vertex]
        except KeyError:
            store = WeakKeyDictionary()
            self.persist[Vertex] = self.side.persist[Vertex] = store
            return store

    def attach(self, other):
        super().attach(other)
        self.persist[Vertex] = self.side.persist[Vertex] = other.vertex

    def detach(self):
        super().detach()
        try:
            del self.persist[Vertex]
            del self.side.persist[Vertex]
        except KeyError:
            pass
