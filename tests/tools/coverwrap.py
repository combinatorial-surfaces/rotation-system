#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["CoverCCWrap"]

from ...core import Flag, Dual
from ...component import ConnectedComponent

class VirtFlag(Flag):
    __slots__ = ("_flag_", "_virtwin_")

    warned = False

    @classmethod
    def _warn_(cls):
        if cls.warned: return
        cls.warned = True
        print("Warning: dual next/prev is not O(1) in the cover")

    def __init__(self, flag, virtwin=False):
        self._flag_ = flag
        self._virtwin_ = virtwin

    def __eq__(self, other):
        if not (isinstance(other, VirtFlag)):
            return NotImplemented
        return (self._flag_ == other._flag_) \
                and (self._virtwin_ == other._virtwin_)

    @property
    def next(self):
        if self._virtwin_: return self
        self._warn_()
        start = self._flag_
        flag = start.next
        while not (flag.is_real or flag == start):
            flag = flag.next
        return type(self)(flag)

    @property
    def prev(self):
        if self._virtwin_: return self
        self._warn_()
        start = self._flag_
        flag = start.prev
        while not (flag.is_real or flag == start):
            flag = flag.prev
        return type(self)(flag)

    @property
    def side(self):
        return type(self)(self._flag_.side, self._virtwin_)

    @property
    def twin(self):
        if self._virtwin_: return type(self)(self._flag_)
        flag = self._flag_
        if flag.is_real: return type(self)(flag.twin)
        return type(self)(flag, True)

    def _persist_(self, namespace):
        return self._flag_._persist_((namespace, self._virtwin_))

    def _repr_(self, classname):
        return "{}({!r}{})".format(classname,
                                 self._flag_,
                                 ", True" if self._virtwin_ else "")

class VirtDual(Dual):
    __slots__ = ()

    def __init__(self, primal):
        if not isinstance(primal, VirtFlag):
            primal = VirtFlag(primal)
        super().__init__(primal)

    def __repr__(self):
        return self.primal._repr_(type(self).__name__)

class CoverCCWrap():
    def __init__(self, cover):
        self.cover = cover

    def __getattr__(self, name):
        return getattr(self.cover, name)

    def __dir__(self):
        s = set(dir(self.cover))
        s.update(super().__dir__())
        return s

    # Special methods that should be pass-through but __getattr__ won't catch

    def __bool__(self):
        return bool(self.cover)

    def __iter__(self):
        return iter(self.cover)

    # Overridden methods

    @property
    def dual(self):
        # ensure the anchor is realized or Virt* walks will never terminate
        self.anchor.twin
        return ConnectedComponent(VirtDual(self.anchor))


