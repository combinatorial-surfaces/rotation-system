#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["NamedFlag"]

from .persist import Vertex
from ...basic import BasicFlag

import collections, itertools

class NamedFlag(Vertex, BasicFlag):
    __slots__ = ()

    freeids = collections.defaultdict(itertools.count)

    @classmethod
    def freename(cls, t, name):
        n = next(cls.freeids[t, name])
        return name if n == 0 and name != "" else "{}-{}".format(name, n)

    def __new__(cls, name="edge", vname="v"):
        flag = super().__new__(cls)
        flag.name = name
        flag.vname = vname
        flag.twin.vname = vname
        return flag

    @property
    def name(self):
        return self.persist.get(NamedFlag) \
                or self.side.persist.get(NamedFlag) \
                or self.twin.name + "⁻¹"
    @name.setter
    def name(self, value):
        for x in (self.side, self.twin, self.twin.side):
            try:
                del x.persist[NamedFlag]
            except KeyError:
                pass
        self.persist[NamedFlag] = self.freename("E", value)

    @property
    def vname(self):
        return self.vertex[NamedFlag]
    @vname.setter
    def vname(self, value):
        self.vertex[NamedFlag] = self.freename("V", value)

    def __repr__(self):
        realside = self._side_ * self.sig
        return "{}({}, {}, {}{})".format(
                type(self).__name__,
                self.vname,
                self.name,
                "+" if self._side_ == 1 else "-",
                "P" if realside == 1 else "N")
