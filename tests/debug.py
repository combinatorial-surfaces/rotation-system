#!/usr/bin/env python3
# vim: spelllang=en_gb

from ..homotopy import *
from ..homotopy.cover import *
from ..homotopy.reduce import *
from ..paths import *

from .tools.info import SurfaceInfo as I
from .tools.named import NamedFlag
from .tools import primitives

from .hyperbolic.geometry.euclidian import *
from .hyperbolic.geometry import *
from .hyperbolic.geometry.robustmath import *

from .debug_gtk import *

import itertools

tube = primitives.tube.named(10,10)
tube.hflag(3,3).sig = -1
tube.hflag(5,5).sig = -1
tube.vflag(7,6).sig = -1

cover = radial_cover(tube.vflag(4,4), Flag=NamedFlag)


w = GtkView(cover)
w.set_title(cover.radial.relator)

def w_reset():
    global w
    w.update(basepoint=Point(-0.36603+4.59622e-06, -0.36603+4.59622e-06),
             angle=Math.pi * (1/6 - 1/4))
w_reset()

def set_band(p, middle=cover.anchor, span=1):
    global w
    n = (len(p) // 4) * 2
    f1 = Path(middle.lift(p[n-1::-1])).end
    f2 = Path(f1.lift(p)).end.lift(p[0])
    def strafe(f):
        p = itertools.islice(f.line(enlarge=500, both_sides=False), span+1)
        f = f.side
        n = itertools.islice(f.line(enlarge=500, both_sides=False), span+1)
        return itertools.chain(p, n)
    l = list(zip(strafe(f1), strafe(f2))) # make a list to enlarge if needed
    shortest = all_shortest_in(cover)
    for a1, a2 in l:
        for f in shortest(a1, a2):
            for x in f.cycle():
                x.twin # realize
    w.band = (f1, f2)
    w.update()

w.band_settings = { "dist": 0.5, "samples": 50 }


"""

lifts = [cover_path]
for _ in range(5):
    lifts.append(Path(lifts[-1].end.lift(radial_path)))


free_line_id = 0
def line_id(flag, create_missing=True):
    global free_line_id
    try:
        return flag.persist[line_id]
    except KeyError:
        if not create_missing: return None
        lid = free_line_id
        for f in flag.line():
            assert(line_id not in f.persist)
            f.persist[line_id] = f.side.persist[line_id] = lid
            f = f.twin; f.persist[line_id] = f.side.persist[line_id] = lid
        free_line_id += 1
        return lid

lines = [ [line_id(c) for c in l] for l in lifts ]

intersect = [False] * len(lifts[3])
for n, c in enumerate(lifts[3]):
    for f in c.line():
        xid = line_id(f.next, False)
        if xid is None: xid = line_id(f.prev, False)
        #print(lines[3][n], xid, lines[4][n])
        if xid == lines[4][n]:
            intersect[lines[3][n]] = True
"""
