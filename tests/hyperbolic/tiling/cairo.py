#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["CairoTiling"]

import math
from . import CoverTiling
from ..render.cairo import CairoRenderer


class CairoTiling():
    def __init__(self, base_or_component, **kwargs):
        super().__init__()
        self.settings = {}
        #
        self.scale = 1
        # Renderers
        self.graph = CairoRenderer()
        self.cells = CairoRenderer()
        self.cells.color = (1,0,0,1)
        self.v_vertices = CairoRenderer()
        self.v_vertices.point_mark = CairoRenderer.MarkType.circle
        self.v_vertices.bg_color = self.v_vertices.color
        self.z_vertices = CairoRenderer()
        self.z_vertices.point_mark = CairoRenderer.MarkType.square
        self.z_vertices.bg_color = (1,1,1,1)
        self.realnums = CairoRenderer()
        self.realnums.point_mark = CairoRenderer.MarkType.text
        self.realnums.color = (0,0,0,1)
        self.realnums.bg_color = (1,1,1,0.5)
        self.realnums.point_size = 15
        self.outernums = CairoRenderer()
        self.outernums.color = (0,0,0,0.4)
        self.outernums.bg_color = (1,1,1,0.5)
        self.outernums.point_mark = CairoRenderer.MarkType.text
        self.outernums.point_size = 15
        self.other = CairoRenderer()
        self.other.color = (0,0,1,1)
        # Generate the tiling
        self.update(base=base_or_component, **kwargs)

    def update(self, base=None, **kwargs):
        if base is None: base = self.tiling.base
        self.settings.update(kwargs)
        self.tiling = CoverTiling(base, **self.settings)
        self.other.clear()
        for name, elts in self.tiling.items():
            try:
                renderer = getattr(self, name)
            except AttributeError:
                renderer = self.other
            else:
                renderer.clear()
            renderer.add(elts)

    def render(self, ctx):
        self.scale_render(ctx, self.scale)

    def scale_render(self, ctx, scale):
        self.scale = scale
        ctx.arc(0, 0, scale, 0, 2*math.pi)
        ctx.set_source_rgba(1,1,1,1)
        ctx.fill_preserve()
        ctx.set_source_rgba(0,0,0,1)
        ctx.set_line_width(1)
        ctx.stroke_preserve()
        ctx.clip()
        self.cells.scale_render(ctx, scale=scale)
        self.graph.scale_render(ctx, scale=scale)
        ctx.save()
        ctx.arc(0, 0, scale * 0.98, 0, 2*math.pi)
        ctx.clip()
        self.v_vertices.scale_render(ctx, scale=scale)
        self.z_vertices.scale_render(ctx, scale=scale)
        self.realnums.scale_render(ctx, scale=scale)
        self.outernums.scale_render(ctx, scale=scale)
        self.other.scale_render(ctx, scale=scale)
        ctx.restore()
