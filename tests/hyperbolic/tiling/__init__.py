#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["CoverTiling"]

from .. import geometry
from ..geometry.euclidian import Point
import collections

class Text(Point):
    __slots__ = ("text")

Elements = collections.namedtuple("Elements",
        "vertex edge cell crossing")
Cell = collections.namedtuple("Cell", "border corners")

class MirrorError(RuntimeError):
    pass

class CoverTiling(dict):
    __eq__ = object.__eq__
    __hash__ = object.__hash__
    __repr__ = object.__repr__

    def elements(self, cf):
        try:
            return cf.persist[self]
        except (AttributeError, KeyError):
            raise ValueError("not a known cover flag") from None

    def __init__(self, base_or_component, *,
                 basepoint=geometry.origin, angle=0):
        try:
            self.base = base = base_or_component.anchor
        except AttributeError:
            self.base = base = base_or_component
        self.basepoint = basepoint
        self.angle = angle
        # create the sets
        self["cells"] = cell_set = set()
        self["graph"] = graph_set = set()
        self["v_vertices"] = v_set = set()
        self["z_vertices"] = z_set = set()
        self["realnums"] = num_set = set()
        self["outernums"] = onum_set = set()
        # generate the first cell
        size = sum(1 for _ in base.cycle())
        cell_points = geometry.pq_polygon(size, 4, basepoint, angle)
        for i, cf in enumerate(base.cycle()):
            corners = cell_points.segment(i)
            border = geometry.geodesic(*corners)
            cell = Cell(border, corners)
            edge = geometry.geodesic(basepoint, basepoint.reflect(border))
            crossing = Text(geometry.intersection(border, edge)[0])
            crossing.text = str(cf.num)
            cf.persist[self] = cf.side.persist[self] = \
                    Elements(basepoint, edge, cell, crossing)
            #
            graph_set.add(edge)
            cell_set.add(border)
            (num_set if cf.is_real else onum_set).add(crossing)
        (z_set if cf.vertex_type == 1 else v_set).add(basepoint)
        # now the others, by mirroring
        def reflect(elt, mirror):
            try:
                # may raise due to cells too close to boundary
                return elt.reflect(mirror)
            except:
                raise MirrorError
        for u in base.connected_component.spanning_tree():
            try:
                v = u.twin.side # reflection is twin.side, not just twin
                mirror = self.elements(u).cell.border
                vertex = reflect(self.elements(u).vertex, mirror)
                # mirror elements by the cell edge
                # as an optimization, just link elements that are globally
                # unchanged by the reflection
                for cf, rcf in zip(u.cycle(), v.cycle()):
                    elts = self.elements(cf)
                    if cf == u:
                        edge = elts.edge
                        crossing = elts.crossing
                    else:
                        edge = reflect(elts.edge, mirror)
                        crossing = reflect(elts.crossing, mirror)
                        crossing.text = str(rcf.num)
                    if cf in (u, u.next, u.prev):
                        cell = elts.cell
                        border = cell.border
                    else:
                        border = reflect(elts.cell.border, mirror)
                        corners = tuple(reflect(P, mirror)
                                        for P in elts.cell.corners)
                        cell = Cell(border, corners)
                    rcf.persist[self] = rcf.side.persist[self] = \
                            Elements(vertex, edge, cell, crossing)
                    #
                    graph_set.add(edge)
                    cell_set.add(border)
                    (num_set if cf.is_real else onum_set).add(crossing)
                (z_set if v.vertex_type == 1 else v_set).add(vertex)
            except MirrorError:
                break
