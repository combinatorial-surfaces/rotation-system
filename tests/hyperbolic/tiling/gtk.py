#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["GtkTiling"]

import cairo, math
from gi.repository import Gtk, Gdk
from .. import geometry
from ..geometry import euclidian
from .cairo import CairoTiling

class GtkTiling(CairoTiling, Gtk.Window):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.zoom = 1
        self.offset = (0,0)
        self.add_events(Gdk.EventMask.POINTER_MOTION_MASK)

    def scale_info(self):
        a = self.get_allocation()
        space = min(a.width, a.height)
        scale = space * 0.9 / 2 * self.zoom
        x, y = self.offset
        return (scale, scale - x + space * 0.05, scale - y + space * 0.05)

    def do_draw(self, ctx):
        Gtk.Window.do_draw(self, ctx)
        a = self.get_allocation()
        ctx.rectangle(a.x, a.x, a.width, a.height)
        ctx.set_source_rgba(1,1,1,1)
        ctx.fill()
        scale, x, y = self.scale_info()
        ctx.translate(x, y)
        ctx.scale(1, -1)
        self.scale_render(ctx, scale)

    def pdf(self, filename=None):
        if filename is None: filename = self.get_title() + ".pdf"
        scale = 500
        pdf = cairo.PDFSurface(filename, scale * 2, scale * 2)
        ctx = cairo.Context(pdf)
        ctx.translate(scale, scale)
        ctx.scale(1, -1)
        self.scale_render(ctx, scale)
        pdf.finish()

    def do_motion_notify_event(self, event):
        scale, x, y = self.scale_info()
        x = (event.x - x) / scale; y = -(event.y - y) / scale
        if event.state & Gdk.ModifierType.BUTTON1_MASK:
            length = (x ** 2 + y ** 2) ** (1/2) / 0.95
            if length > 1:
                x = x / length; y = y / length
            self.update(basepoint = euclidian.Point(x, y))
        elif event.state & Gdk.ModifierType.BUTTON3_MASK:
            vect = (euclidian.Point(x, y) - self.tiling.basepoint).normalize()
            angle = math.acos(vect.x) * (-1 if vect.y < 0 else 1)
            self.update(angle=angle)

    def update(self, **kwargs):
        super().update(**kwargs)
        self.queue_draw()
