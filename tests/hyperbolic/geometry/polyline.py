#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["PolyLine"]

from . import boundary
from .euclidian import *
from .robustmath import Math

class PolyLine(tuple):
    def __new__(cls, elements, *, samples=100):
        try:
            elts = iter(elements)
        except TypeError:
            elts = (elements,)
        if samples < 2: samples = 2
        cos = Math.cos; sin = Math.sin
        L = []
        for element in elts:
            try:
                x, y = element
            except (TypeError, ValueError):
                pass
            else:
                L.append((x, y))
                continue
            if isinstance(element, Point):
                L.append((element.x, element.y))
            elif isinstance(element, Straight):
                inter = intersection(element, boundary)
                if len(inter) == 1:
                    L.append((inter[0].x, inter[0].y))
                elif len(inter) == 2:
                    p1, p2 = inter
                    for t in range(samples):
                        t /= samples - 1
                        x = p1.x * t + (1 - t) * p2.x
                        y = p1.y * t + (1 - t) * p2.y
                        L.append((x, y))
            elif isinstance(element, Circle):
                inter = intersection(element, boundary)
                if len(inter) == 1:
                    L.append((inter[0].x, inter[0].y))
                elif len(inter) == 2:
                    v = (inter[0] - element.center).normalize()
                    a1 = Math.acos(v.x) * (-1 if v.y < 0 else 1)
                    v = (inter[1] - element.center).normalize()
                    a2 = Math.acos(v.x) * (-1 if v.y < 0 else 1)
                    a1, a2 = sorted((a1, a2))
                    v = Vector(cos((a1+a2)/2), sin((a1+a2)/2))
                    mid = element.center + element.radius * v
                    if not boundary.contains(mid): a2 -= 2*Math.pi
                    for t in range(samples):
                        t /= samples - 1
                        angle = a1 * t + (1 - t) * a2
                        v = Vector(cos(angle), sin(angle))
                        p = element.center + element.radius * v
                        L.append((p.x, p.y))
            else:
                raise NotImplementedError
        return super().__new__(cls, L)

