#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["Number", "Math"]

class Number():
    PRECISION = 10
    __slots__ = ("_f_")

    def __new__(cls, f):
        self = super().__new__(cls)
        if isinstance(f, Number): f = f._f_
        self._f_ = float(f)
        return self
    def __float__(self):
        return round(self._f_, self.PRECISION)
    def __int__(self):
        return int(self._f_)
    @property
    def epsilon(self):
        return self._f_ - float(self)
    @property
    def fullprec(self):
        return self._f_
    def __hash__(self):
        return hash(float(self))
    def __eq__(self, other):
        return float(self - other) == 0
    def __ne__(self, other):
        return float(self - other) != 0
    def __gt__(self, other):
        return float(self - other) > 0
    def __ge__(self, other):
        return float(self - other) >= 0
    def __lt__(self, other):
        return float(self - other) < 0
    def __le__(self, other):
        return float(self - other) <= 0
    def __bool__(self):
        return bool(float(self))
    def __repr__(self):
        x = float(self)
        return "{}{:+g}".format(x, self._f_ - x)
    def __format__(self, *a, **kw):
        return format(self._f_, *a, **kw)

    def __pos__(self):
        return type(self)(+self._f_)
    def __neg__(self):
        return type(self)(-self._f_)
    def __abs__(self):
        return type(self)(abs(self._f_))

    def __pow__(self, other):
        if isinstance(other, Number): other = other._f_
        if int(other) != other and self == 0:
            return type(self)(0)
        return type(self)(self._f_ ** other)
    def __rpow__(self, other):
        return type(self)(other) ** self._f_

def create(op):
    def impl(self, other):
        if isinstance(other, Number): other = other._f_
        x = getattr(self._f_, op)(other)
        if x is NotImplemented: return x
        return type(self)(x)
    impl.__name__ = op
    impl.__qualname__ = "Number.{}".format(op)
    setattr(Number, op, impl)
for op in ("__add__", "__sub__", "__mul__",
           "__truediv__", "__floordiv__", "__mod__", "__divmod__",
           "__radd__", "__rsub__", "__rmul__",
           "__rtruediv__", "__rfloordiv__", "__rmod__", "__rdivmod__"):
    create(op)
del create


class Math():
    wrapped = {}
    def __getattribute__(self, func):
        import math, functools
        f = getattr(math, func)
        try:
            return type(self).wrapped[f]
        except KeyError:
            if callable(f):
                    @functools.wraps(f)
                    def w(*a, **kw):
                        a = (getattr(x, "fullprec", x) for x in a)
                        kw = {k: getattr(v, "fullprec", v)
                              for k, v in kw.items()}
                        result = f(*a, **kw)
                        try:
                            return Number(result)
                        except:
                            return result
                    type(self).wrapped[f] = w
                    return w
            try:
                return Number(f)
            except:
                return f
Math = Math()
