#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["origin", "boundary",
           "HyCircle", "geodesic", "Polygon", "pq_polygon",
           "intersection", "distance"]

from .robustmath import *
from .euclidian import *
euclidian_intersection = intersection

origin = Point(0,0)
boundary = Circle(origin, 1)

def distance(point1, point2):
    OPs = (point1 - origin) ** 2
    OQs = (point2 - origin) ** 2
    PQs = (point2 - point1) ** 2
    return Math.acosh(1 + 2 * PQs / (1 - OPs) / (1 - OQs))

class HyCircle(Circle):
    __slots__ = ("hycenter", "hyradius")

    def __new__(cls, hycenter, radius_or_point):
        if isinstance(radius_or_point, Point):
            hyradius = distance(hycenter, radius_or_point)
        else:
            hyradius = Number(radius_or_point)
        vect = hycenter - origin
        OP = vect.length
        dist = Math.tanh(hyradius / 2 + Math.atanh(OP))
        if OP == 0:
            self = super().__new__(cls, hycenter, dist)
            self.hyradius = hyradius
            self.hycenter = hycenter
            return self
        P = origin + dist / OP * vect
        reflection = hycenter.reflect(boundary) # cannot raise since OP != 0
        geodesic = Circle.from_diameter(hycenter, reflection)
        eucl_center = Point.middle(P, P.reflect(geodesic))
        self = super().__new__(cls, eucl_center, P)
        self.hycenter = hycenter
        self.hyradius = hyradius
        return self

    def __repr__(self):
        return ("{0.__class__.__name__}({0.hycenter!r}, {0.hyradius!r}) "
                "# euclidian ({0.center.x:g}, {0.center.x:g}), "
                "{0.radius:g}".format(self))

_typed_geodesics_ = {Straight: [], Circle: []}

class geodesic():
    __slots__ = ()

    def __eq__(self, other):
        if not isinstance(other, geodesic):
            return NotImplemented
        eq = super().__eq__(other)
        # when self and other are not both Circles or both Straights,
        # eq is NotImplemented, but we should return False
        return (eq == True)

    def __repr__(self):
        if isinstance(self, Straight):
            n = "Straight"
        else:
            n = "Circle"
        s = super().__repr__()
        return "<{}: {}{}>".format(type(self).__name__, n, s[s.index("("):])

    def __hash__(self):
        return super().__hash__()

    @property
    def idealpoints(self):
        return euclidian_intersection(self, boundary)

    @classmethod
    def _make_(cls, elt, *a, **kw):
        if issubclass(cls, elt):
            t = cls
        else:
            try:
                t = next(d for c, d in _typed_geodesics_[elt] if c is cls)
            except StopIteration:
                class t(cls, elt):
                    __slots__ = ()
                t.__name__ = cls.__name__
                t.__qualname__ = cls.__qualname__
                _typed_geodesics_[elt].append((cls, t))
        return elt.__new__(t, *a, **kw)

    def __new__(cls, point, tangent_or_point):
        if isinstance(tangent_or_point, Point):
            v = tangent_or_point - point
            nullerror = "geodesic needs two different points"
        elif isinstance(tangent_or_point, Vector):
            v = tangent_or_point
            nullerror = "a tangent vector cannot be null"
        else:
            raise TypeError(tangent_or_point) from None
        try:
            v = v.normalize()
        except ValueError:
            raise ValueError(nullerror) from None
        try:
            P = point.reflect(boundary)
        except ValueError:
            # 1st point is the origin, geodesic will be a line
            return cls._make_(Straight, v.orthogonal())
        P = Point.middle(point, P)
        L1 = Straight(P - boundary.center, P) # perpendicular trough P
        if isinstance(tangent_or_point, Point):
            try:
                Q = tangent_or_point.reflect(boundary)
            except ValueError:
                # 2nd point is the origin
                return cls._make_(Straight, v.orthogonal())
            Q = Point.middle(tangent_or_point, Q)
            L2 = Straight(Q - boundary.center, Q) # perpendicular trough Q
        else:
            # L2 will be perpendicular to the geodesic, so its normal
            # vector should be the desired tangent vector
            L2 = Straight(v, point)
        try:
            I, = euclidian_intersection(L1, L2)
        except ValueError:
            # no intersection, geodesic is the line through O and P
            v = (P - origin).orthogonal()
            return cls._make_(Straight, v)
        return cls._make_(Circle, I, point)

class Polygon(tuple):
    __slots__ = ()

    def segment(self, n):
        return (self[n % len(self)], self[(n+1) % len(self)])

    def reflect(self, mirror):
        return Polygon(p.reflect(mirror) for p in self)

class pq_polygon(Polygon):
    __slots__ = ()

    def __new__(cls, p, q, center=origin, angle=0):
        cos = Math.cos; sin = Math.sin; pi = Math.pi
        angle = angle % (2 * pi)
        hr = 2 * Math.atanh(Math.sqrt(
                cos(pi/p + pi/q)*cos(pi/q)
                / (sin(2*pi/q) * sin(pi/p) + cos(pi/p + pi/q)* cos(pi/q))
            ))
        C = HyCircle(center, hr)
        points = []
        for n in range(p):
            v = Vector(cos(angle), sin(angle))
            geo = geodesic(center, v)
            cur = next(P for P in euclidian_intersection(C, geo)
                         if (P-center) * v > 0)
            points.append(cur)
            angle += 2 * pi / p
        return super().__new__(cls, points)

class intersection(tuple):
    __slots__ = ()

    def __new__(cls, obj1, obj2):
        return super().__new__(cls,
                (P for P in euclidian_intersection(obj1, obj2)
                   if boundary.contains(P)))
