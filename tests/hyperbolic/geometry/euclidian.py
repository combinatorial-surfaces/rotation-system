#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["Vector", "Point", "Straight", "Circle", "intersection"]

from .robustmath import Number

class VectorPoint():
    __slots__ = ("x", "y")

    def __new__(cls, x, y=None):
        self = super().__new__(cls)
        if y is None: x, y = x.x, x.y
        self.x = Number(x)
        self.y = Number(y)
        return self

    def __repr__(self):
        return "{0.__class__.__name__}({0.x!r}, {0.y!r})".format(self)

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))

class Vector(VectorPoint):
    __slots__ = ()

    def __neg__(self):
        return type(self)(-self.x, -self.y)

    def __pos__(self):
        return self

    def __add__(self, other):
        if not isinstance(other, Vector): return NotImplemented
        return type(self)(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        if not isinstance(other, Vector): return NotImplemented
        return type(self)(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        if not isinstance(other, Vector): return NotImplemented
        return self.x * other.x + self.y * other.y

    def __rmul__(self, scalar):
        return type(self)(self.x * scalar, self.y * scalar)

    def __pow__(self, other):
        if other != 2: return NotImplemented
        return self * self

    @property
    def length(self):
        p = self * self
        return p ** (type(p)(1) / 2) # sqrt with correct type

    def normalize(self):
        length = self.length
        zero = type(length)(0)
        if length == zero: return type(self)(zero, zero)
        return type(self)(self.x / length, self.y / length)

    def orthogonal(self):
        return type(self)(-self.y, self.x)

    @staticmethod
    def det(v1, v2):
        return v1.x * v2.y - v1.y * v2.x

class Point(VectorPoint):
    __slots__ = ()

    def __add__(self, vector):
        if not isinstance(vector, Vector): return NotImplemented
        return type(self)(self.x + vector.x, self.y + vector.y)

    def __sub__(self, point):
        if not isinstance(point, Point): return NotImplemented
        return Vector(self.x - point.x, self.y - point.y)

    def translate(self, vector):
        return self + vector

    @staticmethod
    def middle(A, B):
        return A + 1/2 * (B - A)

    def reflect(self, mirror):
        if isinstance(mirror, Straight):
            factor = (self - mirror.base) * mirror.vect
            return self + (-2 * factor * mirror.vect)
        elif isinstance(mirror, Circle):
            vect = self - mirror.center
            distsq = vect ** 2
            if distsq == 0:
                raise ValueError("point sent to infinity")
            factor = mirror.radius ** 2 / distsq - 1
            return self + factor * vect

class Straight():
    __slots__ = ("vect", "dist")

    def __new__(cls, vect, dist_or_base=0):
        self = super().__new__(cls)
        try:
            length = vect.length
        except AttributeError:
            raise TypeError(vect) from None
        if length == 0:
            raise ValueError("a normal vector cannot be null")
        self.vect = vect.normalize()
        if isinstance(dist_or_base, Point):
            self.dist = self.vect * (dist_or_base - Point(0,0))
        else:
            self.dist = Number(dist_or_base) * length
        # ensure the first non-null is positive
        if (self.dist or self.vect.x or self.vect.y) < 0:
            self.vect = -self.vect
            self.dist = -self.dist
        return self

    def __repr__(self):
        return "{0.__class__.__name__}({0.vect!r}, {0.dist!r})".format(self)

    def __eq__(self, other):
        if not isinstance(other, Straight):
            return NotImplemented
        return self.vect == other.vect and self.dist == other.dist

    def __hash__(self):
        return hash((self.vect, self.dist))

    def same_side(self, P, Q):
        P_dir = (P - self.base) * self.vect > 0
        Q_dir = (Q - self.base) * self.vect > 0
        return P_dir == Q_dir

    @property
    def base(self):
        return Point(0,0) + (self.dist * self.vect)

    @property
    def director(self):
        return Vector(-self.vect.y, self.vect.x)

    def normal(self, point):
        return self.vect

    def translate(self, vector):
        return type(self)(self.vect, self.base + vector)

    def reflect(self, mirror):
        if isinstance(mirror, Straight):
            factor = self.vect * mirror.vect
            newvect = self.vect - 2 * factor * mirror.vect
            newbase = self.base.reflect(mirror)
            return Straight(newvect, newbase)
        elif isinstance(mirror, Circle):
            factor = (self.base - mirror.center) * self.vect
            if factor == 0: return self
            newfactor = mirror.radius ** 2 / factor / 2
            newcenter = mirror.center + newfactor * self.vect
            return Circle(newcenter, abs(newfactor))

class Circle():
    __slots__ = ("center", "radius")

    def __new__(cls, center, radius_or_point):
        self = super().__new__(cls)
        self.center = center
        if isinstance(radius_or_point, Point):
            self.radius = (radius_or_point - center).length
        else:
            self.radius = Number(radius_or_point)
        return self

    def __repr__(self):
        return "{0.__class__.__name__}" \
                "({0.center!r}, {0.radius!r})".format(self)

    def __eq__(self, other):
        if not isinstance(other, Circle):
            return NotImplemented
        return self.center == other.center and self.radius == other.radius

    def __hash__(self):
        return hash((self.center, self.radius))

    def contains(self, point):
        return (point - self.center).length < self.radius

    def same_side(self, P, Q):
        return self.contains(P) == self.contains(Q)

    def normal(self, point):
        return point - self.center

    @classmethod
    def from_diameter(cls, A, B):
        ray = 1/2 * (B - A)
        return Circle.__new__(cls, A + ray, ray.length)

    @classmethod
    def through(cls, p1, p2, p3):
        "Returns the circle (or straight line) passing though the 3 points."
        L1 = Straight(p2 - p1, Point.middle(p1, p2))
        L2 = Straight(p2 - p3, Point.middle(p3, p2))
        try:
            I, = intersection(L1, L2)
        except ValueError:
            # no intersection, points are aligned, return the line
            return Straight((p3 - p1).orthogonal(), p2)
        return Circle.__new__(cls, I, p2)

    def reflect(self, mirror):
        if isinstance(mirror, Straight):
            return Circle(self.center.reflect(mirror), self.radius)
        elif isinstance(mirror, Circle):
            vect = self.center - mirror.center
            diff = vect ** 2 - self.radius ** 2
            if diff == 0:
                # self is passing through the inversion center. Its image is
                # a line passing through the inversion of the point on self
                # farthest from the inversion center, and normal to the ray.
                through = (mirror.center + 2 * vect).reflect(mirror)
                return Straight(vect, through)
            factor = mirror.radius ** 2 / diff
            return Circle(mirror.center + factor * vect,
                          abs(factor) * self.radius)


class intersection(tuple):
    __slots__ = ()

    @classmethod
    def _make_(cls, *objects):
        return super().__new__(cls, objects)

    def __new__(cls, obj1, obj2):
        if isinstance(obj1, Straight):
            if isinstance(obj2, Straight):
                return cls.lines(obj1, obj2)
            return cls.line_circle(obj1, obj2)
        elif isinstance(obj2, Straight):
            return cls.__new__(cls, obj2, obj1)
        return cls.circle_circle(obj1, obj2)

    @classmethod
    def lines(cls, l1, l2):
        det = Vector.det(l1.vect, l2.vect)
        if det == 0:
            return cls._make_() # parallel
        factor1 = (l2.base - Point(0,0)) * l2.vect / det
        factor2 = (Point(0,0) - l1.base) * l1.vect / det
        p = Point(0,0) + factor1 * l1.director + factor2 * l2.director
        return cls._make_(p)

    @classmethod
    def line_circle(cls, line, circle):
        vect = circle.center - line.base
        factor = vect * line.vect
        base = line.base + (vect - factor * line.vect)
        distsq = circle.radius ** 2 - factor ** 2
        if distsq < 0: return cls._make_() # no intersection
        dist = distsq ** (type(distsq)(1)/2)
        return cls._make_(base + dist * line.director,
                                     base + (-dist) * line.director)

    @classmethod
    def circle_circle(cls, c1, c2):
        vect = c2.center - c1.center
        distsq = vect ** 2
        if distsq <= (c1.radius - c2.radius) ** 2:
            return cls._make_() # one circle is into the other
        factor = ((c1.radius ** 2 - c2.radius ** 2) / distsq + 1) / 2
        base = c1.center + factor * vect
        return cls.line_circle(Straight(vect, base), c1)
