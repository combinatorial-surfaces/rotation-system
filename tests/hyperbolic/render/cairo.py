#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["CairoRenderer"]

from ..geometry.euclidian import *
from ..geometry.polyline import PolyLine
from . import *

import cairo, math

class CairoRenderer(Renderer):
    def render(self, context):
        self.context = context
        context.save()
        context.set_source_rgba(*self.color)
        context.set_line_width(self.line_width)
        context.select_font_face("Sans",
                                 cairo.FontSlant.NORMAL,
                                 cairo.FontWeight.NORMAL)
        context.set_font_size(self.point_size)
        context.new_path()
        try:
            for element in list(self):
                if isinstance(element, Point):
                    self.render_point(element)
                elif isinstance(element, Straight):
                    self.render_straight(element)
                elif isinstance(element, Circle):
                    self.render_circle(element)
                elif isinstance(element, PolyLine):
                    self.render_poly(element)
                else:
                    raise NotImplementedError(element)
        finally:
            context.restore()
            del self.context

    def render_point(self, element):
        ctx = self.context
        x = element.x * self.scale; y = element.y * self.scale
        size = self.point_size / 2
        m_t = self.point_mark
        if m_t is self.MarkType.cross:
            ctx.move_to(x - size, y - size)
            ctx.line_to(x + size, y + size)
            ctx.move_to(x + size, y - size)
            ctx.line_to(x - size, y + size)
            ctx.stroke()
        elif m_t in (self.MarkType.circle, self.MarkType.square):
            if m_t is self.MarkType.circle:
                ctx.arc(x, y, size, 0, 2 * math.pi)
            else:
                ctx.rectangle(x - size, y - size, 2*size, 2*size)
            ctx.save()
            ctx.set_source_rgba(*self.bg_color)
            ctx.fill_preserve()
            ctx.restore()
            ctx.stroke()
        elif m_t is self.MarkType.text:
            try:
                text = element.text
            except AttributeError:
                raise ValueError("'text' mark type with no text attribute")
            ctx.save()
            x, y = ctx.user_to_device(x, y)
            ctx.identity_matrix()
            ext = ctx.text_extents(text)
            x = x - ext.x_bearing - ext.width / 2
            y = y - ext.y_bearing - ext.height / 2
            ctx.rectangle(x - 0.1 * ext.width, y + 0.1 * ext.width,
                          1.2 * ext.width, -1.2 * ext.height)
            ctx.save()
            ctx.set_source_rgba(*self.bg_color)
            ctx.fill()
            ctx.restore()
            ctx.move_to(x, y)
            ctx.show_text(text)
            ctx.restore()
        else:
            raise NotImplementedError

    def render_straight(self, element):
        ctx = self.context; scale = self.scale
        b = element.base; bx = b.x * scale; by = b.y * scale
        vx = -element.vect.y * scale * 2; vy = element.vect.x * scale * 2
        ctx.move_to(bx - vx, by - vy)
        ctx.line_to(bx + vx, by + vy)
        ctx.stroke()

    def render_circle(self, element):
        ctx = self.context; scale = self.scale
        x = element.center.x * scale; y = element.center.y * scale
        r = element.radius * scale
        ctx.arc(x, y, r, 0, 2 * math.pi)
        ctx.stroke()

    def render_poly(self, element):
        ctx = self.context; scale = self.scale
        for x, y in element:
            ctx.line_to(x * scale, y * scale)
        ctx.stroke()
