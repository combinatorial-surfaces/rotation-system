#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["Renderer"]

import enum

class Renderer(set):
    class MarkType(enum.Enum):
        def __repr__(self):
            return "{}.{}".format(type(self).__name__, self.name)
        cross = object()
        circle = object()
        square = object()
        text = object() # searches for a |text| attribute

    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.color = (0,0,0,1)
        self.bg_color = (1,1,1,1)
        self.point_mark = self.MarkType.cross
        self.point_size = 5
        self.line_width = 1
        self.scale = 1

    def add(self, *objects):
        try:
            objects, = objects
        except ValueError:
            pass
        try:
            objects = iter(objects)
        except TypeError:
            objects = (objects,)
        return self.update(objects)

    def render(self, *a, **kw):
        raise NotImplementedError

    def scale_render(self, *a, scale=None, **kw):
        if scale is None:
            raise ValueError
        self.scale = scale
        self.render(*a, **kw)
