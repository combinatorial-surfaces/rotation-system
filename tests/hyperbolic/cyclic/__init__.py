#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["BandTiling"]

from .. import geometry
from ..geometry import euclidian
from ..geometry.polyline import PolyLine

import math

def gen_transform(line1, point1, line2, point2, neg_P=None):
    shortest = geometry.geodesic(point1, point2)
    point1, *_ = geometry.intersection(line1, shortest)
    point2, *_ = geometry.intersection(line2, shortest)
    I1, I2 = shortest.idealpoints
    if neg_P is None:
        neg_P = point1 + (point2 - point1).orthogonal()
    base_dist = geometry.distance(point1, point2)
    def transform(P):
        circle = euclidian.Circle.through(I1, P, I2)
        H1, *_ = geometry.intersection(line1, circle)
        H2, *_ = geometry.intersection(line2, circle)
        perp = geometry.geodesic(H1, circle.normal(H1))
        B1, *_ = geometry.intersection(perp, shortest)
        perp = geometry.geodesic(H2, circle.normal(H2))
        B2, *_ = geometry.intersection(perp, shortest)
        perp = geometry.geodesic(P, circle.normal(P))
        B, *_ = geometry.intersection(perp, shortest)
        d = geometry.distance(B1, B)
        D = geometry.distance(B1, B2)
        # distance along the hypercircle is d * cosh(r)
        # so y = d * cosh(r) / ( D * cosh(r) ) = d / D
        y = d / D
        if not line1.same_side(P, H2): y *= -1
        t = min(max(0, y), 1)
        x1 = geometry.distance(point1, H1)
        x2 = geometry.distance(point2, H2)
        x = (1 - t) * x1 + t * x2
        if shortest.same_side(P, neg_P): x *= -1
        transform.perp = perp
        transform.circle = circle
        transform.shortest = shortest
        return type(P)(x / base_dist, y)
    return transform

class BandTiling(dict):
    __eq__ = object.__eq__
    __hash__ = object.__hash__
    __repr__ = object.__repr__

    def __init__(self, tiling, flag1, flag2, *,
                 samples=100, dist=0.1, band_only=True,
                 callback=None):
        info1 = tiling.elements(flag1)
        info2 = tiling.elements(flag2)
        line1 = info1.cell.border; line2 = info2.cell.border
        point1 = info1.crossing; point2 = info2.crossing
        transform = gen_transform(line1, point1, line2, point2,
                                  tiling.elements(flag1.next).crossing)
        total = sum(len(s) for s in tiling.values())
        max_x = 0
        count = 0
        for name, elt_set in tiling.items():
            new_set = self[name] = []
            for elt in elt_set:
                count += 1
                if isinstance(elt, euclidian.Point):
                    new_elt = transform(elt)
                    try:
                        new_elt.text = elt.text
                    except AttributeError:
                        pass
                else:
                    sampled = [euclidian.Point(x,y)
                               for x,y in PolyLine(elt, samples=samples)]
                    decimated = []
                    for p in sampled:
                        try:
                            d = geometry.distance(decimated[-1], p)
                        except:
                            pass # decimated is empty or math error
                        else:
                            if d < dist: continue
                        decimated.append(p)
                    if not band_only:
                        filtered = decimated
                    else:
                        filtered = []
                        replacepos = 0
                        for p in decimated:
                            is_in = (line1.same_side(p, point2) and
                                    line2.same_side(p, point1))
                            if is_in:
                                filtered.append(p)
                                replacepos = len(filtered)+1
                            else:
                                filtered[replacepos:] = [p]
                    transformed = []
                    for p in filtered:
                        try:
                            P = transform(p)
                        except Exception:
                            continue
                        max_x = max(max_x, P.x, -P.x)
                        transformed.append(P)
                    new_elt = PolyLine(transformed)
                try:
                    callback(count, total, new_elt)
                except:
                    pass
                new_set.append(new_elt)
        cells = self.setdefault("cells", [])
        cells.append(PolyLine(((-max_x, 0), (max_x, 0))))
        cells.append(PolyLine(((-max_x, 1), (max_x, 1))))
