#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["GtkBandTiling"]

import cairo, math
from gi.repository import Gtk, Gdk
from .. import geometry
from ..geometry import euclidian
from .cairo import CairoBandTiling

class GtkBandTiling(CairoBandTiling, Gtk.Window):
    def __init__(self, *args, **kwargs):
        self.updating = None
        super().__init__(*args, **kwargs)
        self.zoom = 1
        self.offset = (0,0)
        self.add_events(Gdk.EventMask.POINTER_MOTION_MASK)

    def scale_info(self):
        a = self.get_allocation()
        scale = a.height / self.count * self.zoom
        x, y = self.offset
        return (scale, a.width / 2 - x, a.height - y)

    def do_draw(self, ctx):
        Gtk.Window.do_draw(self, ctx)
        a = self.get_allocation()
        ctx.rectangle(a.x, a.x, a.width, a.height)
        ctx.set_source_rgba(1,1,1,1)
        ctx.fill()
        if self.updating is not None:
            c, t, *_ = self.updating
            ctx.select_font_face("Sans",
                                 cairo.FontSlant.NORMAL,
                                 cairo.FontWeight.NORMAL)
            ctx.set_font_size(10)
            ctx.set_source_rgba(0,0,0,1)
            ctx.move_to(a.x + a.width / 2, a.y + a.height / 2)
            ctx.show_text("{}/{} = {:.1%}".format(c, t, c/t))
            return
        scale, x, y = self.scale_info()
        ctx.translate(x, y)
        ctx.scale(1, -1)
        self.scale_render(ctx, scale)

    def pdf(self, filename=None):
        if filename is None: filename = self.get_title() + ".pdf"
        scale = 300
        pdf = cairo.PDFSurface(filename, scale * 3, self.count * scale)
        ctx = cairo.Context(pdf)
        ctx.translate(scale * 1.5, 6 * scale)
        ctx.scale(1, -1)
        self.scale_render(ctx, scale)
        pdf.finish()

    def do_motion_notify_event(self, event):
        pass

    def _callback_(self, *info):
        self.updating = info
        self.queue_draw()
        Gtk.main_iteration_do(False)

    def update(self, *args, **kwargs):
        kwargs["callback"] = self._callback_
        super().update(*args, **kwargs)
        self.updating = None
        self.queue_draw()
