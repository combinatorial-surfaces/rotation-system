#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["CairoBandTiling"]

import math
from . import BandTiling
from ..render.cairo import CairoRenderer


class CairoBandTiling():
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.settings = {}
        self.count = 6
        #
        self.scale = 1
        # Renderers
        self.graph = CairoRenderer()
        self.cells = CairoRenderer()
        self.cells.color = (1,0,0,1)
        self.v_vertices = CairoRenderer()
        self.v_vertices.point_mark = CairoRenderer.MarkType.circle
        self.v_vertices.bg_color = self.v_vertices.color
        self.z_vertices = CairoRenderer()
        self.z_vertices.point_mark = CairoRenderer.MarkType.square
        self.z_vertices.bg_color = (1,1,1,1)
        self.realnums = CairoRenderer()
        self.realnums.point_mark = CairoRenderer.MarkType.text
        self.realnums.color = (0,0,0,1)
        self.realnums.bg_color = (1,1,1,0.5)
        self.realnums.point_size = 15
        self.outernums = CairoRenderer()
        self.outernums.color = (0,0,0,0.4)
        self.outernums.bg_color = (1,1,1,0.5)
        self.outernums.point_mark = CairoRenderer.MarkType.text
        self.outernums.point_size = 15
        self.other = CairoRenderer()
        self.other.color = (0,0,1,1)
        # Generate the tiling
        self.update(*args, **kwargs)

    def update(self, tiling=None, flag1=None, flag2=None, **kwargs):
        if tiling is not None: self.settings["tiling"] = tiling
        if flag1 is not None: self.settings["flag1"] = flag1
        if flag2 is not None: self.settings["flag2"] = flag2
        self.settings.update(kwargs)
        self.band = BandTiling(**self.settings)
        self.other.clear()
        for name, elts in self.band.items():
            try:
                renderer = getattr(self, name)
            except AttributeError:
                renderer = self.other
            else:
                renderer.clear()
            renderer.add(elts)

    def render(self, ctx):
        self.scale_render(ctx, self.scale)

    def scale_render(self, ctx, scale):
        self.scale = scale
        for i in range(self.count):
            ctx.save()
            ctx.translate(0, i*scale)
            ctx.rectangle(-5 * scale, 0, 10 * scale, scale)
            ctx.clip()
            self.cells.scale_render(ctx, scale=scale)
            self.graph.scale_render(ctx, scale=scale)
            self.v_vertices.scale_render(ctx, scale=scale)
            self.z_vertices.scale_render(ctx, scale=scale)
            self.realnums.scale_render(ctx, scale=scale)
            self.outernums.scale_render(ctx, scale=scale)
            self.other.scale_render(ctx, scale=scale)
            ctx.restore()
