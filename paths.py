#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["Path", "shortest_from", "all_shortest_in"]

class Path(list):
    @property
    def start(self):
        return self[0]

    @property
    def end(self):
        return self[-1].twin

    def __reversed__(self):
        return (f.twin for f in super().__reversed__(self))

    def invert(self):
        return type(self)(reversed(self))

    def shorten(self):
        if not self: return type(self)
        end = self.end
        s = shortest_from(self[0], stop=end) #stop building the tree early
        return type(self)(s(end))

    def __repr__(self):
        return "{}({})".format(type(self).__name__, super().__repr__())


class shortest_from():
    __slots__ = ("_source_", "__weakref__")

    def __init__(self, source, *, stop=None):
        try:
            source = source.connected_component
        except AttributeError:
            pass
        anchor = self._source_ = source.anchor
        for f in anchor.cycle():
            f.persist[self] = f.side.persist[self] = None
        for v in source.spanning_tree():
            for f in v.twin.cycle():
                f.persist[self] = f.side.persist[self] = v
                if f == stop or f.side == stop: return

    def __call__(self, dest):
        try:
            dest = dest.persist[self]
        except KeyError:
            raise ValueError("no path to destination") from None
        path = []
        while dest is not None:
            path.append(dest)
            dest = dest.persist[self]
        return Path(reversed(path))


class all_shortest_in():
    # TODO: less naive implementation, this one is O(graph size^2) complexity
    #       and space for initialization, O(shortest length) afterwards
    __slots__ = ("__weakref__")

    def __init__(self, comp):
        try:
            comp = comp.connected_component
        except AttributeError:
            pass
        # memorize a (number, list) couple for all vertices
        for i, v in enumerate(comp.vertices()):
            val = (i, [])
            for f in v.cycle():
                f.persist[self] = f.side.persist[self] = val
        # fill-in the lists with backpath information
        for source in comp.vertices():
            source.persist[self][1].append(None)
            for v in source.connected_component.spanning_tree():
                v.twin.persist[self][1].append(v)

    def __call__(self, source, dest):
        try:
            i, _ = source.persist[self]
        except KeyError:
            raise ValueError("unknown source") from None
        try:
            l = dest.persist[self]
        except KeyError:
            raise ValueError("no path to destination") from None
        dest = l[1][i]
        path = []
        while dest is not None:
            path.append(dest)
            dest = dest.persist[self][1][i]
        return Path(reversed(path))
