#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["BasicFlag"]

from .core import Flag
from weakref import WeakKeyDictionary

class BasicFlag(Flag):
    """A Flag implementation in pure Python based on edges with signatures

    Behind a BasicFlag is an underlying edge, that it shares with its
    twin and side. That 'edge' is divided in two half-edges, one for
    each endpoint, that have some thickness, and thus two sides. Every
    BasicFlag remembers the corresponding 'half-edge' on one hand, and
    the 'side' that it represents with a value in ±1. The 'twin' of a
    flag always has the same side value as itself.
    Normally, in the canonical rotation order around a vertex, the
    negative side of any half-edge is encountered first, then its
    positive side (in particular, the positive side of an half-edge is
    generally facing the negative side of it's 'next', which means
    that the side value of 'flag.next' is equal to that of 'flag').
    That canonical orientation can be changed by setting the
    'signature' of the edge endpoint to '-1' instead of '1'.

    If both signatures of an edge (that is 'flag.sig' and 'flag.twin.sig')
    are different, then the edge is 'twisted', which is typical of graphs
    cellularly embedded in non-orientable surfaces."""

    __slots__ = ("_half_", "_side_")

    def __new__(cls):
        new = super().__new__(cls)
        new._side_ = 1
        half = new._half_ = {}
        half[1] = half[-1] = half
        twin = half["twin"] = { "twin": half }
        twin[1] = twin[-1] = twin
        half["sig"] = twin["sig"] = 1
        return new

    def _make_(self, *, half=None, side=None):
        new = super().__new__(type(self))
        new._half_ = half or self._half_
        new._side_ = side or self._side_
        return new

    def __eq__(self, other):
        if not isinstance(other, BasicFlag):
            return NotImplemented
        return (self._half_ is other._half_) \
                and (self._side_ == other._side_)

    def __hash__(self):
        return hash((id(self._hash_), self._side_))

    def __repr__(self):
        half = self._half_
        realside = self._side_ * half["sig"]
        return "<{}: {}, {}{}>".format(type(self).__name__,
                                       hex(id(half)),
                                       "+" if self._side_ == 1 else "-",
                                       "P" if realside == 1 else "N")

    @property
    def sig(self):
        return self._half_["sig"]
    @sig.setter
    def sig(self, value):
        if not (value == 1 or value == -1):
            raise ValueError
        self._half_["sig"] = value

    @property
    def next(self):
        half = self._half_
        direction = self._side_ * half["sig"]
        next_half = self._half_[direction]
        next_side = direction * next_half["sig"]
        return self._make_(half = next_half, side = next_side)

    # use the default Flag implementation of 'prev' that relies on 'next'

    @property
    def twin(self):
        return self._make_(half = self._half_["twin"]) # same side

    @property
    def side(self):
        return self._make_(side = -self._side_) # same half-edge

    def attach(self, other):
        "Attach 'self' in the 'next' position of the 'other' flag\n\n"
        "'self.sig' will be switched if needed so that 'other.next' can be "
        "equal to 'self'."
        if not isinstance(other, BasicFlag):
            raise TypeError("attach: cannot attach '{}' object to '{}' "
                            "object".format(type(self).__name__,
                                            type(other).__name__))
        half = self._half_
        ohalf = other._half_
        if not half[1] is half:
            raise ValueError("attach: '{}' object is not "
                             "detached".format(type(self).__name__))
        direction = other._side_ * ohalf["sig"]
        nhalf = half[direction] = ohalf[direction]
        ohalf[direction] = nhalf[-direction] = half
        half[-direction] = ohalf
        half["sig"] = self._side_ * direction

    def detach(self):
        "Detach 'self' from the graph"
        half = self._half_
        half[1][-1] = half[-1]
        half[-1][1] = half[1]
        half[-1] = half[1] = half

    def contract(self):
        "Contract the edge, merging both ends neighbour cycles"
        to_merge = list(self.twin.cycle())[1:] # ignore self.twin
        for he in reversed(to_merge):
            he.detach()
            he.attach(self) # will switch signature if needed
        self.detach()

    def _persist_(self, namespace):
        key = "persist:{}".format(self._side_)
        store = self._half_.setdefault(key, {})
        return store.setdefault(namespace, WeakKeyDictionary())
