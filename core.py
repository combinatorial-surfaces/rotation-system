#!/usr/bin/env python3
# vim: spelllang=en_gb

__all__ = ["Flag", "Dual"]

class Flag():
    """The interface that all Flags should implement.

    SOME INVARIANTS THAT SHOULD BE RESPECTED:
     * self.twin.twin == self.side.side == self
     * self.twin.side == self.side.twin
     * self.next.prev == self.prev.next == self
     * self.side.next == self.prev.side
     * self.side.prev == self.next.side
    BY CONVENTION:
     * self.twin.side borders the same face than self
     * self.next.side borders the same face than self"""

    __slots__ = ()
    __hash__ = None

    @property
    def next(self):
        "The next flag in the circular neighbourhood"
        raise NotImplementedError("'{}' object should implement "
                "next and prev".format(type(self).__name__))

    @property
    def prev(self):
        "The previous flag in the circular neighbourhood"
        return self.side.next.side

    @property
    def twin(self):
        "The opposite flag along the edge"
        raise NotImplementedError("'{}' object should implement "
                "the twin involution".format(type(self).__name__))

    @property
    def side(self):
        "The flag representing the other face of the half-edge"
        raise NotImplementedError("'{}' object should implement "
                "the side involution".format(type(self).__name__))

    def cycle(self):
        """Iterate along the neighbour cycle around the vertex.
        Since it just gets flag.next repeatedly, only one side of each flag
        is yielded."""
        flag = self
        while True:
            yield flag
            flag = flag.next
            if flag == self: break

    def face(self):
        """Iterate along the face bordered by the flag.
        Unless the face is non-orientable, at most one of 'flag' and
        'flag.side' can border the face for any given flag."""
        flag = self
        while True:
            yield flag
            flag = flag.twin.prev
            if flag == self: break

    def attached(self):
        "Returns True iff there is another half-edge around the vertex"
        return self.next != self

    def detached(self):
        "Returns True iff there is no other half-edge around the vertex"
        return self.next == self

    @property
    def connected_component(self):
        "A class for diverse walk-based algorithms of self's connected "
        "component. Will never be an empty ConnectedComponent."
        return ConnectedComponent(self)

    @property
    def dual(self):
        """The flag corresponding to self in the dual graph.
        self.dual.dual is self."""
        return Dual(self)

    @property
    def persist(self):
        """A short-term persistent MutableMapping for algorithmic usage.

        This mapping is shared between all users. The Flag shall not keep a
        strong reference to the keys so as to not keep data alive forcibly.
        As soon as a key object is unreferenced and collected, the Flag shall
        forget about the corresponding data. Keys must be hashable, but in
        addition it must be possible to create a weak reference to any key.
        This essentially mandates that 'flag.persist' behaves like a
        WeakKeyDictionnary.

        The intended use is to store information in
        'flag.persist[user]' where 'user' is an object whose lifetime
        corresponds to the scope where the data is needed."""
        return self._persist_("")

    def _persist_(self, namespace):
        """A short-term persistent MutableMapping for the given namespace.

        This mapping is shared between all users of the given namespace, but
        all namespaces shall be independent. As for 'flag.persist', each
        'flag._persist_(namespace)' shall behave like a WeakKeyDictionnary.

        By default 'flag.persist' is 'flag._persist_("")'."""
        raise NotImplementedError("'{}' object should provide persistent "
                "weak key dictionnaries".format(type(self).__name__))

from .component import ConnectedComponent

class Dual(Flag):
    "Dynamically computed Flag along the dual graph"

    __slots__ = ("primal",)

    def __init__(self, primal):
        if not isinstance(primal, Flag):
            raise TypeError("impossible to create a dual of '{}' "
                    "object".format(type(primal).__name__))
        self.primal = primal

    def __eq__(self, other):
        if not isinstance(other, Dual):
            return NotImplemented
        return self.primal == other.primal

    def __repr__(self):
        return "{}({!r})".format(type(self).__name__, self.primal)

    @property
    def next(self):
        return type(self)(self.primal.twin.prev)

    @property
    def prev(self):
        return type(self)(self.primal.next.twin)

    @property
    def twin(self):
        return type(self)(self.primal.twin)

    @property
    def side(self):
        return type(self)(self.primal.twin.side)

    @property
    def dual(self):
        return self.primal

    def _persist_(self, namespace):
        return self.primal._persist_(("dual", namespace))
